<?php 
	require "./../fonctions/bdd.php";
	include("../fonctions/statusCheck.php");
 ?>

<!DOCTYPE html>
<html>

<head>
    <title>Création de voyage</title>
    <link rel="icon" type="image/png" href="./../img/logoFavicon.png" />
    <link rel="stylesheet" type="text/css" href="../style/creationVoyage.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</head>

<body>
    <?php include "./../include/navbar.php"?>
    </br>

    <fieldset class='jumbotron'>

        <form method="post" action="./../fonctions/traitementCreation.php">

            <label for="nomVoyage">Nom du voyage</label>
            <input type="text" id="nomVoyage" name="nomVoyage" class="form-control" maxlength="256" required>
            </br>

            <label for="dateDebut">Date de début</label>
            <input type="date" id="dateDebut" name="dateDebut" class="form-control" required>
        </br>
        
        <label for="dateFin">Date de fin</label>
        <span id="dateCache">La date de fin du voyage est inférieur à la date de début</span>
            <input type="date" id="dateFin" name="dateFin" class="form-control" required>
            </br>

            <label for="localisation">Ville</label>
            <input type="text" id="localisation" name="localisation" maxlength="256" class="form-control" required>
            </br>

            <button type="submit" id="creationVoyage" class="btn btn-outline-success">Valider ce voyage</button>
        </form>
    </fieldset>

    <script type="text/javascript" src="./../js/creationVoyage.js"></script>
</body>

</html>