<?php
session_start();
if (isset($_SESSION['connected'])) {
    $_SESSION['connected'] = false;
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>MyTravel</title>
    <link rel="icon" type="image/png" href="./../img/logoFavicon.png" />
    <link rel="stylesheet" type="text/css" href="../style/connexion.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</head>

<body>
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">

        <div class="d-md-flex d-block flex-row mx-md-auto mx-0">
            <img src="../img/logo.png" width="50" height="50" class="d-inline-block align-top" alt="">
            MyTravel
        </div>
        </div>
    </nav>
    <div id="contenu">
        <div id="connexion" class="jumbotron">
            <legend>
                <center>Connexion</center>
            </legend>
            <form method="post" action="./../fonctions/traitementConnexion.php">
                <div class="form-group ">
                    <?php
if (isset($_GET['loginerror']) && $_GET['loginerror']) {
    echo "<span>Nom d'utilisateur ou mot de passe incorrect</span></br>";
}
?>
                    <label for="nomUtilisateurConnexion">Nom d'utilisateur</label>
                    <input type="text" class="form-control" id="nomUtilisateurConnexion" name="nomUtilisateurConnexion"
                        maxlength="256" required>
                    <label for="mdpConnexion" class="col-form-label">Mot de passe</label>
                    <input type="password" class="form-control" id="mdpConnexion" name="mdpConnexion" maxlength="256"
                        required>
                </div>
                <button type="submit" class="btn btn-outline-success">Se connecter</button>
            </form>
        </div>

        <div id="inscription" class="jumbotron">
            <legend>
                <center>Inscription </center>
            </legend>
            <form method="post" action="./../fonctions/traitementConnexion.php">
                <div class="form-group">
                    <label for="nomUtilisateur">Nom utilisateur</label></br>
                    <?php
if (isset($_GET['erreur']) && $_GET['erreur']) {
    echo "<span>Le nom d'utilisateur existe déjà.</span>";
}
?>
                    <span id="utilisateurCache">Le nom d'utilisateur est trop court</span>
                    <input type="text" class="form-control" id="nomUtilisateur" name="nomUtilisateur" maxlength="256"
                        required>
                    <label for="mail">Adresse mail</label></br>
                    <span id="mailCache">L'adresse mail n'est pas valide</span>
                    <input type="mail" class="form-control" id="mail" name="mail" maxlength="256" required>
                    <label for="naissance" class="col-form-label">Date de naissance</label>
                    <input type="date" class="form-control" id="naissance" name="naissance"
                        max=<?php echo date("Y-m-d"); ?> required>
                    <label for="mdp" class="col-form-label">Mot de passe</label></br>
                    <span id="mdpCache">Le mot de passe est trop court</span>
                    <input type="password" class="form-control" id="mdp" name="mdp" maxlength="256" required>
                    <label for="confirmation" class="col-form-label">Confirmation du mot de passe</label></br>
                    <span id="identiqueCache">Le mot de passe de confirmation est différent du mot de passe</span>
                    <input type="password" class="form-control" id="confirmation" name="confirmation" maxlength="256"
                        required>

                </div>
                <button type="submit" id="btnInscription" class="btn btn-outline-success">S'inscrire</button>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="./../js/connexion.js"></script>
</body>

</html>