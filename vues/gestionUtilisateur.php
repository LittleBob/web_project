<?php
//require "./../fonctions/bdd.php";
include "../fonctions/statusCheck.php";
?>
<!DOCTYPE html>
<html>

<head>
    <title>Paramètres</title>
    <link rel="icon" type="image/png" href="./../img/logoFavicon.png" />
    <link rel="stylesheet" type="text/css" href="../style/gestionUtilisateur.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</head>

<body>
    <?php include "./../include/navbar.php"?>
    <center>
        <div id="changementMdp" class="jumbotron">
            <legend id="titreMdp">
                <center>Changer votre mot de passe</center>
            </legend>
            <form method="post" class="test" action="./../fonctions/traitementUtilisateur.php">
                <span id="mdpCache">Les mots de passe sont différents</span>
                <span id="mdpCacheTC">Le mot de passe est trop court</span>

                <label for="nouveauMdp">Nouveau mot de passe</label>
                <input type="password" id="nouveauMdp" name="nouveauMdp" maxlength="256" required>

                <label for="confMdp">Confirmation mot de passe</label>
                <input type="password" id="confMdp" name="confMdp" maxlength="256" required>

                <button type="submit" class="btn btn-outline-success" id="validationFormulaire"
                    disabled>Valider</button>
            </form>
            <hr>
            </hr>
            <form method="post" onSubmit="return verif();" action="./../fonctions/traitementSuppressionUtilisateur.php">
                <a><button id="desac" class="btn btn-outline-danger">Désactiver son compte</button></a>
            </form>
        </div>
    </center>



    <script type="text/javascript" src="./../js/gestionUtilisateur.js"></script>
</body>

</html>