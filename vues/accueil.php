<?php 
	require "./../fonctions/bdd.php";
	include("../fonctions/statusCheck.php");
 ?>

<!DOCTYPE html>
<html>

<head>
    <title>Mes voyages</title>
    <link rel="icon" type="image/png" href="./../img/logoFavicon.png" />
    <link rel="stylesheet" type="text/css" href="../style/accueil.css" />
    <script type="text/javascript" src="./../js/accueil.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</head>

<body>
    <?php include "./../include/navbar.php"?>
    </br>
    </br>
    <div id="mesVoyages">
        <!-- <div class="voyages"> -->
        <?php
			$bdd = Bdd::getBdd();
			$req=$bdd->getAllTrips($_SESSION['id']);
			foreach($req as $row)
			{
				echo "<fieldset class='jumbotron voyages'>";
				if($_SESSION['id']==$row['creator_id']){
					echo "<div class='display-4 nomVoyage'>" . $row['trip_name'] . " (créateur)" . "</div><br/>";
				}else{
					echo "<div class='display-4 nomVoyage'>" . $row['trip_name'] . " (participant)" . "</div><br/>";
				}
					
				echo "<div class='display-6'>Date du début du voyage : " . $row['date_beginning'] . "</div><br/>";
				echo "<div class='display-5'>Date de fin du voyage : " . $row['date_end'] . "</div><br/>";
				echo "<div class='display-6'>Ville : " . $row['location'] . "</div><br/>";

				echo "<button class='btn btn-info'><a href='./votes.php?id=". $row['trip_id'] ."'>Voir les votes</a></button>";
				echo "<button class='btn btn-info'><a href='./tchat.php?id=". $row['trip_id'] ."'>Voir le tchat associé</a></button>";
				if($_SESSION['id']==$row['creator_id']){
					
					echo "<button class='btn btn-info'><a href='./administrationVoyage.php?id=". $row['trip_id'] ."'>Administrer</a></button><br/>";
				}else{
					echo "<form action='./../fonctions/traitementSuppressionUtilisateurVoyage.php?id=". $row['trip_id'] ."' method='post' onSubmit='return validationSuppressionParticipant();'>";
					echo "<button type='submit' class='btn btn-info dark-text' >Se retirer du voyage</button></form><br/>";
				}


				echo "</fieldset>";
			}

		?>
        <!-- </div> -->
    </div>


</body>

</html>