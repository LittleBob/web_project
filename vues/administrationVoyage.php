<?php
include "../fonctions/statusCheck.php";
require "../fonctions/verification.php";
if (administration($_GET['id'], $_SESSION['id'])) {
    ?>

<!DOCTYPE html>
<html>

<head>
    <title>Administration</title>
    <link rel="icon" type="image/png" href="./../img/logoFavicon.png" />
    <link rel="stylesheet" type="text/css" href="../style/administrationVoyage.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <script type="text/javascript" src="./../js/administrationVoyage.js"></script>
</head>

<body>
    <?php include "./../include/navbar.php"?>
    <?php
//récupération du nom du voyage
    $bdd = Bdd::getBdd();
    $req = $bdd->getNameTrip($_GET['id']);
    $idVoyage = $_GET['id'];
    ?>
    <!-- Partie 1 CSS : ajout éléments voyage -->

    <div id="gestionVoyage">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link active" href="#" onclick="changeTabs(event, 'etape')">Étape</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" onclick="changeTabs(event, 'activite')">Activité</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" onclick="changeTabs(event, 'hebergement')">Hébergement</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" onclick="changeTabs(event, 'transport')">Transport</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" onclick="changeTabs(event, 'participants')">Participants</a>
            </li>
            <li class="nav-item ml-auto">
                <form method="post" onSubmit="return validationSuppression();"
                    action="./../fonctions/traitementSuppressionVoyage.php?id=<?php echo $idVoyage; ?>">
                    <a class="nav-link" href="#"><button type="submit" class="btn btn-outline-danger">Supprimer ce
                            voyage</button></a>
                </form>
            </li>
        </ul>

        <div class="tabcontent" id="etape">
            <div class='jumbotron'>
                <legend>Ajouter une étape</legend>
                <form method="post" id="formEtape"
                    action="./../fonctions/traitementGestion.php?id=<?php echo $idVoyage; ?>">

                    <label for="nomEtape">Nom de l'étape*</label>
                    <input type="text" id="nomEtape" name="nomEtape" class="form-control" maxlength="128" required>

                    <label for="dateDebutEtape">Date de début*</label>
                    <input type="date" id="dateDebutEtape" class="form-control" name="dateDebutEtape" required>

                    <label for="dateFinEtape">Date de fin*</label>
                    <span  id="dateEtapeCache">La date de fin de l'étape est inférieur à la date de début</span>
                    <input type="date" id="dateFinEtape" class="form-control" name="dateFinEtape" required>

                    <button type="submit" class="btn btn-outline-success" id="ajoutEtape">Ajouter étape</button>
                </form>

                <!-- Suppression étape -->
                <hr>
                </hr>
                <legend>Supprimer une étape</legend>
                <form method="post" onSubmit="return validationSuppressionEtape();"
                    action="./../fonctions/traitementSuppressionElements.php?id=<?php echo $idVoyage; ?>">
                    <select type="text" id="etapeSuppression" class="form-control" name="etapeSuppression" required>
                        <?php
                            $bdd = Bdd::getBdd();
                            $req = $bdd->getAllSteps($_GET['id']);
                            foreach ($req as $row) {
                                echo "<option value='" . $row['step_id'] . "'>";
                                echo $row['step_name']." : " .$row['date_beginning']." - ".$row['date_end'] ;
                                echo "</option>";
                            }
                        ?>
                    </select>

                    <button type="submit" class="btn btn-outline-danger">Supprimer cette étape</button>
                </form>
            </div>
        </div>

        <div class="tabcontent" id="activite">
            <div class='jumbotron'>
                <form method="post" action="./../fonctions/traitementGestion.php?id=<?php echo $idVoyage; ?>">
                    <legend>Ajouter une activité</legend>
                    <label for="nomActivite">Nom*</label>
                    <input type="text" id="nomActivite" name="nomActivite" class="form-control" required>

                    <label for="etapeActivite">Étape*</label>
                    <select type="text" id="etapeActivite" name="etapeActivite" class="form-control" required>
                        <?php
                            $bdd = Bdd::getBdd();
                            $req = $bdd->getAllSteps($_GET['id']);
                            foreach ($req as $row) {
                                echo "<option value='" . $row['step_id'] . "'>";
                                echo $row['step_name']." : " .$row['date_beginning']." - ".$row['date_end'] ;
                                echo "</option>";
                            }
                        ?>
                    </select>

                    <label for="typeActivite">Type*</label>
                    <input type="text" id="typeActivite" name="typeActivite" class="form-control" required>

                    <label for="dateDebutActivite">Date de début*</label>
                    <span  id="verificationDateActivite">La date de l'activité n'est pas comprise dans les dates de l'étape</span>
                    <input type="date" id="dateDebutActivite" name="dateDebutActivite" class="form-control" required>

                    <label for="dateFinActivite">Date de fin*</label>
                    <span  id="dateActiviteCache">La date de fin de l'activité est inférieur à la date de début</span>
                    <input type="date" id="dateFinActivite" name="dateFinActivite" class="form-control" required>

                    <label for="descriptionActivite">Description*</label>
                    <input type="text" id="descriptionActivite" name="descriptionActivite" class="form-control"
                        required>

                    <!-- traiter en css -->
                    <label for="prixActivite">Prix</label>
                    <input type="number" min="0" id="prixActivite" name="prixActivite" class="form-control">

                    <button type="submit" id="ajoutActivite" class="btn btn-outline-success">Ajouter activité</button>
                </form>
                <hr>
                </hr>
                <legend>Supprimer une activité</legend>
                <form method="post" onSubmit="return validationSuppressionElement('cette activité');"
                    action="./../fonctions/traitementSuppressionElements.php?id=<?php echo $idVoyage; ?>">
                    <select type="text" id="activiteSuppression" class="form-control" name="activiteSuppression"
                        required>
                        <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getActivities($_GET['id']);
    foreach ($req as $row) {
        echo "<option value='" . $row['activity_id'] . "'>";
        echo $row['activity_name'];
        echo "</option>";
    }
    ?>
                    </select>

                    <button type="submit" class="btn btn-outline-danger">Supprimer cette activité</button>
                </form>
            </div>
        </div>


        <div class="tabcontent" id="hebergement">
            <div class="jumbotron">
                <legend>Ajouter un hébergement</legend>
                <form method="post" action="./../fonctions/traitementGestion.php?id=<?php echo $idVoyage; ?>">

                    <label for="nomHebergement">Nom*</label>
                    <input type="text" id="nomHebergement" class="form-control" name="nomHebergement" required>

                    <label for="etapeHebergement">Étape*</label>
                    <select type="text" id="etapeHebergement" class="form-control" name="etapeHebergement" required>
                        <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getAllSteps($_GET['id']);
    foreach ($req as $row) {
        echo "<option value='" . $row['step_id'] . "'>";
        echo $row['step_name']." : " .$row['date_beginning']." - ".$row['date_end'] ;
        echo "</option>";
    }
    ?>
                    </select>

                    <label for="typeHebergement">Type*</label>
                    <input type="text" id="typeHebergement" class="form-control" name="typeHebergement" maxlength="64"
                        required>

                    <label for="dateDebutHebergement">Date de début*</label>
                    <span id="verificationDateHebergement">La date de l'hébergement n'est pas comprise dans les dates de l'étape</span>
                    <input type="date" id="dateDebutHebergement" class="form-control" name="dateDebutHebergement"
                        required>

                    <label for="dateFinHebergement">Date de fin*</label>
                    <span id="dateHebergementCache">La date de fin de l'hébergement est inférieur à la date de
                        début</span>
                    <input type="date" id="dateFinHebergement" class="form-control" name="dateFinHebergement" required>

                    <label for="locationHebergement">Location*</label>
                    <input type="text" id="locationHebergement" class="form-control" name="locationHebergement"
                        maxlength="256" required>

                    <label for="descriptionHebergement">Description*</label>
                    <input type="text" id="descriptionHebergement" class="form-control" name="descriptionHebergement"
                        required>

                    <!-- traiter en css -->
                    <label for="prixHebergement">Prix</label>
                    <input type="number" min="0" class="form-control" id="prixHebergement" name="prixHebergement">

                    <button type="submit" id="ajoutHebergement" class="btn btn-outline-success">Ajouter
                        hébergement</button>
                </form>
                <!-- Suppression hébergement -->
                <hr>
                </hr>
                <legend>Supprimer un hébergement</legend>
                <form method="post" onSubmit="return validationSuppressionElement('cet hébergement');"
                    action="./../fonctions/traitementSuppressionElements.php?id=<?php echo $idVoyage; ?>">
                    <select type="text" id="hebergementSuppression" class="form-control" name="hebergementSuppression"
                        required>
                        <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getAccomodations($_GET['id']);
    foreach ($req as $row) {
        echo "<option value='" . $row['accomodation_id'] . "'>";
        echo $row['accomodation_name'];
        echo "</option>";
    }
    ?>
                    </select>

                    <button type="submit" class="btn btn-outline-danger">Supprimer cet hébergement</button>
                </form>
            </div>
        </div>


        <div class="tabcontent" id="transport">
            <div class="jumbotron">
                <legend>Ajouter un transport</legend>
                <form method="post" action="./../fonctions/traitementGestion.php?id=<?php echo $idVoyage; ?>">

                    <label for="origineTransport">Origine*</label>
                    <input type="text" id="origineTransport" class="form-control" name="origineTransport"
                        maxlength="128" required>

                    <label for="destinationTransport">Destination*</label>
                    <input type="text" id="destinationTransport" class="form-control" name="destinationTransport"
                        maxlength="128" required>

                    <label for="typeTransport">Type*</label>
                    <input type="text" id="typeTransport" class="form-control" name="typeTransport" maxlength="64"
                        required>

                    <label for="dateDebutTransport">Date de début*</label>
                    <span  id="verificationDateTransport">La date du transport n'est pas comprise dans les dates de l'étape</span>
                    <input type="date" id="dateDebutTransport" class="form-control" name="dateDebutTransport" required>

                    <label for="dateFinTransport">Date de fin*</label>
                    <span  id="dateTransportCache">La date de fin du transport est inférieur à la date de début</span>
                    <input type="date" id="dateFinTransport" class="form-control" name="dateFinTransport" required>

                    <label for="descriptionTransport">Description*</label>
                    <input type="text" id="descriptionTransport" class="form-control" name="descriptionTransport"
                        required>

                    <!-- traiter en css -->
                    <label for="departTransport">Heure de départ</label>
                    <input type="time" id="departTransport" class="form-control" name="departTransport">

                    <!-- traiter en css -->
                    <label for="arriveeTransport">Heure d'arrivée</label>
                    <input type="time" id="arriveeTransport" class="form-control" name="arriveeTransport">

                    <!-- traiter en css -->
                    <label for="prixTransport">Prix</label>
                    <input type="number" min="0" class="form-control" id="prixTransport" name="prixTransport">

                    <!-- traiter en css -->
                    <label for="etapeTransport">Étape</label>
                    <select type="text" id="etapeTransport" class="form-control" name="etapeTransport" default="">
                        <option value="" selected></option>
                        <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getAllSteps($_GET['id']);
    foreach ($req as $row) {
        echo "<option value='" . $row['step_id'] . "'>";
        echo $row['step_name']." : " .$row['date_beginning']." - ".$row['date_end'] ;
        echo "</option>";
    }
    ?>
                    </select>

                    <button type="submit" id="ajoutTransport" class="btn btn-outline-success">Ajouter transport</button>
                </form>
                <!-- Suppression transport -->
                <hr>
                </hr>
                <legend>Supprimer un transport</legend>
                <form method="post" onSubmit="return validationSuppressionElement('ce transport');"
                    action="./../fonctions/traitementSuppressionElements.php?id=<?php echo $idVoyage; ?>">
                    <select type="text" id="transportSuppression" class="form-control" name="transportSuppression"
                        required>
                        <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getTransports($_GET['id']);
    foreach ($req as $row) {
        echo "<option value='" . $row['transport_id'] . "'>";
        echo $row['type'];
        echo "</option>";
    }
    ?>
                    </select>

                    <button type="submit" class="btn btn-outline-danger">Supprimer ce transport</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Partie gestion participants : partie 2 CSS -->

    <!-- Liste des participants -->
    <div class="tabcontent" id="participants">
        <div class="jumbotron">
            <legend>Liste des participants :</legend>
            <table class='table table-striped'>
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getUsersInTrip($idVoyage);
    foreach ($req as $row) {
        echo "<tr><td>" . $row['user_name'] . "</td><td>" . $row['mail'] . "</td></tr>";
    }
    ?>
                </tbody>
            </table>
            <!-- ajout participant au voyage -->

            <legend>Ajouter un participant</legend>
            <form method="post" action="./../fonctions/traitementGestion.php?id=<?php echo $idVoyage; ?>">
                <select type="text" id="ajoutParticipant" class="form-control" name="ajoutParticipant" required>
                    <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getUsersInTripAddable($idVoyage, $_SESSION['id']);
    foreach ($req as $row) {
        echo "<option value='" . $row['user_id'] . "'>";
        echo $row['user_name'];
        echo "</option>";
    }
    ?>
                </select>

                <button type="submit" class="btn btn-outline-success">Ajouter</button>
            </form>
            <hr>
            </hr>
            <legend>Supprimer un participant</legend>
            <form method="post" onSubmit="return validationSuppressionElement('ce participant');"
                action="./../fonctions/traitementGestion.php?id=<?php echo $idVoyage; ?>">
                <select type="text" id="suppressionParticipant" class="form-control" name="suppressionParticipant"
                    required>
                    <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getUsersInTrip($idVoyage);
    foreach ($req as $row) {
        echo "<option value='" . $row['user_id'] . "'>";
        echo $row['user_name'];
        echo "</option>";
    }
    ?>
                </select>

                <button type="submit" class="btn btn-outline-danger">Supprimer</button>
            </form>
        </div>
    </div>
    
</body>

</html>

<?php

} else {
    header('Location: ./accueil.php');
}
?>