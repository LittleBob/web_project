<?php 
	include("../fonctions/statusCheck.php");
	require "../fonctions/verification.php";
	if(votes($_GET['id'],$_SESSION['id'])){
 ?>

<!DOCTYPE html>
<html>

<head>
    <title>Tchat</title>
    <link rel="icon" type="image/png" href="./../img/logoFavicon.png" />
    <link rel="stylesheet" type="text/css" href="../style/tchat.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <script type="text/javascript" src="./../js/tchat.js"></script>
</head>

<body>
    <?php include "./../include/navbar.php"?>
    <div id='affichageMessages'>
    </div>

    <!-- Insertion de message -->
    <div class="col-lg-6 mw-100">
        <div class="input-group">
            <input type="text" class="form-control" id="message" name="message" placeholder="Exprimez-vous !"
                maxlength="256">
            <span class="input-group-btn">
                <button class="btn btn-primary" id="ajoutMessage" type="button">Envoyer</button>
            </span>
        </div>
    </div>

</body>

</html>

<?php

	}
	else{
		header('Location: ./accueil.php');
	}
?>