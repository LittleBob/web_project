<?php
include "../fonctions/statusCheck.php";
require "../fonctions/verification.php";
if (votes($_GET['id'], $_SESSION['id'])) {
    ?>

<!DOCTYPE html>
<html>

<head>
    <title>Votes</title>
    <link rel="icon" type="image/png" href="./../img/logoFavicon.png" />
    <link rel="stylesheet" type="text/css" href="../style/votes.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="./../js/affichageVotes.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</head>

<body>
    <?php include "./../include/navbar.php"?>
    <div id="listeEtapes">
        <?php
$bdd = Bdd::getBdd();
    $req = $bdd->getTrip($_SESSION['id'], $_GET["id"]);
    if (!$req) {
        header('Location: ../vues/accueil.php');
    }

    $stepsId = $bdd->getTripSteps($_GET["id"]);?>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Étapes</span>
            </div>
            <select id='stepSelect' name='step_id' class='form-control'>


                <?php foreach ($stepsId as $step_id) {
        echo "<option value=" . $step_id["step_id"] . ">" . $step_id["step_name"] . "</option>";
    }
    echo "</select>";
    ?>
        </div>
    </div>
    <div id="step_detail"></div>

</body>

</html>
<?php

} else {
    header('Location: ./accueil.php');
}
?>