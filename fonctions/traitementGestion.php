<?php

    include("./statusCheck.php");
    require "./bdd.php";

    if (isset($_POST['nomEtape']) && isset($_POST['dateDebutEtape']) && isset($_POST['dateFinEtape'])) {
        $nomEtape = $_POST['nomEtape'];
        $dateDebutEtape = $_POST['dateDebutEtape'];
        $dateFinEtape = $_POST['dateFinEtape'];
        $bdd = Bdd::getBdd();
        $bdd->addStep($nomEtape, $_GET['id'], $dateDebutEtape, $dateFinEtape);
        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }
    
    
    else if (isset($_POST['nomActivite']) && isset($_POST['etapeActivite']) && isset($_POST['dateDebutActivite']) && isset($_POST['dateFinActivite']) && isset($_POST['descriptionActivite']) && isset($_POST['typeActivite'])) {
        $nomActivite = $_POST['nomActivite'];
        $etapeActivite = $_POST['etapeActivite'];
        $descriptionActivite = $_POST['descriptionActivite'];
        $typeActivite = $_POST['typeActivite'];
        $dateDebutActivite = $_POST['dateDebutActivite'];
        $dateFinActivite = $_POST['dateFinActivite'];
        $bdd = Bdd::getBdd();
        $prix=null;
        if(isset($_POST['prixActivite']) && $_POST['prixActivite']!=null){
            $prix=$_POST['prixActivite'];
        }
        $bdd->addActivity($nomActivite, $etapeActivite, $typeActivite, $dateDebutActivite, $dateFinActivite, $descriptionActivite,$prix);
        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }

    else if (isset($_POST['nomHebergement']) && isset($_POST['etapeHebergement']) && isset($_POST['dateDebutHebergement']) && isset($_POST['dateFinHebergement']) && isset($_POST['descriptionHebergement']) && isset($_POST['typeHebergement']) && isset($_POST['locationHebergement'])) {
        $nomHebergement = $_POST['nomHebergement'];
        $etapeHebergement = $_POST['etapeHebergement'];
        $descriptionHebergement = $_POST['descriptionHebergement'];
        $typeHebergement = $_POST['typeHebergement'];
        $dateDebutHebergement = $_POST['dateDebutHebergement'];
        $dateFinHebergement = $_POST['dateFinHebergement'];
        $locationHebergement = $_POST['locationHebergement'];
        $prix=null;
        $bdd = Bdd::getBdd();
        if(isset($_POST['prixHebergement']) && $_POST['prixHebergement'] != null){
            $prix=$_POST['prixHebergement'];
        }
        $bdd->addAccomodation($nomHebergement, $etapeHebergement, $typeHebergement, $dateDebutHebergement, $dateFinHebergement, $locationHebergement, $descriptionHebergement,$prix);
        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }

    else if (isset($_POST['origineTransport']) && isset($_POST['destinationTransport']) && isset($_POST['dateDebutTransport']) && isset($_POST['dateFinTransport']) && isset($_POST['descriptionTransport']) && isset($_POST['typeTransport'])) {
        $origineTransport = $_POST['origineTransport'];
        $destinationTransport = $_POST['destinationTransport'];
        $descriptionTransport = $_POST['descriptionTransport'];
        $typeTransport = $_POST['typeTransport'];
        $dateDebutTransport = $_POST['dateDebutTransport'];
        $dateFinTransport = $_POST['dateFinTransport'];
        $departTransport = null;
        $arriveeTransport = null;
        $prixTransport = null;
        $etapeTransport = null;
        $bdd = Bdd::getBdd();
        if(isset($_POST['departTransport']) && $_POST['departTransport'] != null){
            $departTransport = $_POST['departTransport'];
        }
        if(isset($_POST['arriveeTransport']) && $_POST['arriveeTransport'] != null){
            $arriveeTransport = $_POST['arriveeTransport'];
        }
        if(isset($_POST['prixTransport']) && $_POST['prixTransport'] != null){
            $prixTransport = $_POST['prixTransport'];
        }
        if(isset($_POST['etapeTransport']) && $_POST['etapeTransport'] != ""){
            $etapeTransport = $_POST['etapeTransport'];
        }
        $bdd->addTransport($origineTransport, $typeTransport, $destinationTransport, $descriptionTransport, $dateDebutTransport, $dateFinTransport, $departTransport, $arriveeTransport, $prixTransport, $etapeTransport);
        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }
    


    else if (isset($_POST['ajoutParticipant'])) {
        $participant=$_POST['ajoutParticipant'];
        $bdd = Bdd::getBdd();
        $bdd->addUserInTrip($participant, $_GET['id']);
        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }

    else if (isset($_POST['suppressionParticipant'])) {
        $participant=$_POST['suppressionParticipant'];
        $bdd = Bdd::getBdd();
        $bdd->deleteUserInTrip($participant, $_GET['id']);
        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }
   


    else{
        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }
?>