<?php
    //gerer suppression du compte

    require ("./bdd.php");
    include("./statusCheck.php");

    if(isset($_POST['nouveauMdp']) && isset($_POST['confMdp'])){
        $nouveau=$_POST['nouveauMdp'];
        $ancien=$_POST['confMdp'];
        if($nouveau==$ancien){           
           $bdd=Bdd::getBdd();
           $bdd->changePassword($_SESSION['id'],$nouveau);
           header('Location: ../vues/accueil.php'); 
        }
        else{
            //Cas où les deux mots de passe sont différents
            //a gerer en js
            header('Location: ../vues/gestionUtilisateur.php');
        }     
    }
    else{
       header('Location: ../vues/gestionUtilisateur.php');
    }
?>