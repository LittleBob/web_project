<?php

class Bdd
{
    // Défini les attributs
    private static $server = 'mysql:host=mysql-webproject.alwaysdata.net';
    private static $bdd = 'dbname=webproject_bdd';
    private static $user = '202291_user';
    private static $pwd = 'Motdepasse123.';
    private static $bddConnection; //On stock la connexion à la bdd
    private static $bddObject = null; //On stock l'objet Bdd afin de créer une seule connexion à la bdd

    private function __construct()
    {
        try {
            Bdd::$bddConnection = new PDO(Bdd::$server . ';' . Bdd::$bdd, Bdd::$user, Bdd::$pwd); // Création d'une connexion à la base de données
            Bdd::$bddConnection->query("SET CHARACTER SET utf8");
            date_default_timezone_set("Europe/Paris");
        } catch (PDOException $ex) {
            die('Impossible de joindre la base de données');
        }
    }
    // Obtention de la connexion à la base de données
    public static function getBdd()
    {
        if (Bdd::$bddObject == null) { // Si aucune connexion existe alors on en créé une sinon on retourne celle existante
            Bdd::$bddObject = new Bdd();
        }
        return Bdd::$bddObject;
    }

    //Recupere tous les voyages associes a un utilisateur
    public function getAllTrips($user_id)
    {
        $sql = 'SELECT DISTINCT trips.* FROM trips LEFT JOIN users_trips AS ut ON ut.trip_id = trips.trip_id WHERE trips.creator_id = :id OR ut.user_id = :id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $user_id));
        return $result->fetchAll();
    }
    //Recupere un voyage
    public function getTrip($user_id, $trip_id)
    {
        $sql = 'SELECT DISTINCT trips.* FROM trips LEFT JOIN users_trips AS ut ON trips.trip_id = ut.trip_id WHERE (trips.creator_id = :userId OR ut.user_id = :userId) AND trips.trip_id = :tripId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":userId" => $user_id, ":tripId" => $trip_id));
        return $result->fetch();
    }
    //Recupere les étapes d'un voyage
    public function getTripSteps($trip_id)
    {
        $sql = 'SELECT * FROM steps WHERE trip_id = :tripId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":tripId" => $trip_id));
        return $result->fetchAll();
    }
    //Recupere les id accomodations de toutes une étape
    public function getAccomodationsByStep($step_id)
    {
        $sql = 'SELECT COUNT(accomodations_votes.accomodation_id) as vote, accomodations.* FROM accomodations LEFT JOIN accomodations_votes ON accomodations_votes.accomodation_id = accomodations.accomodation_id WHERE step_id = :stepId GROUP BY accomodations.accomodation_id ORDER BY vote DESC';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":stepId" => intval($step_id)));
        return $result->fetchAll();
    }
    //Recupere les id transports de toutes une étape
    public function getTransportsByStep($step_id)
    {
        $sql = 'SELECT COUNT(transports_votes.transport_id) as vote, transports.* FROM transports LEFT JOIN transports_votes ON transports_votes.transport_id = transports.transport_id WHERE step_id = :stepId GROUP BY transports.transport_id ORDER BY vote DESC';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":stepId" => intval($step_id)));
        return $result->fetchAll();
    }
    //Recupere les id activites de toutes une étape
    public function getActivitiesByStep($step_id)
    {
        $sql = 'SELECT COUNT(activities_votes.activity_id) as vote, activities.* FROM activities LEFT JOIN activities_votes ON activities_votes.activity_id = activities.activity_id WHERE step_id = :stepId GROUP BY activities.activity_id ORDER BY vote DESC';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":stepId" => intval($step_id)));
        return $result->fetchAll();
    }
    //Recupere les infos d'une etape
    public function getStepByStep($step_id)
    {
        $sql = 'SELECT COUNT(steps_votes.step_id) as vote, steps.* FROM steps LEFT JOIN steps_votes ON steps_votes.step_id = steps.step_id WHERE steps.step_id = :stepId GROUP BY steps.step_id ORDER BY vote DESC';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":stepId" => intval($step_id)));
        return $result->fetch();
    }

    //Suppression d'un voyage
    public function deleteTrip($trip_id)
    {
        $sql = 'DELETE FROM `trips` WHERE trip_id = :tripId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':tripId' => $trip_id));
    }

    //Ajout un voyage
    public function addTrip($name, $date_beginning, $date_end, $location, $creator_id)
    {
        $sql = 'INSERT INTO trips (`trip_name`, `date_beginning`, `date_end`, `location`, `creator_id`) VALUES (:tripname, :dob, :de, :loc, :creator_id)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':tripname' => $name, ':dob' => $date_beginning, ':de' => $date_end, ':loc' => $location, ':creator_id' => $creator_id));
    }

    //Connexion
    public function connect($username, $password)
    {
        $sql = "SELECT count(*) as count, `user_name`, `password` FROM `users` WHERE `user_name` = :userName";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userName' => $username));
        $user = $result->fetch();
        if ($user["count"] > 0) {
            if (password_verify($password, $user["password"])) {
                return true;
            }
        }
        return false;
    }
    //Inscription
    public function register($username, $email, $password, $birth)
    {
        $sql = "SELECT count(*) as count FROM `users` WHERE `user_name` = :userName OR `mail` = :mail";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userName' => $username, ':mail' => $email));
        $exist = $result->fetch();
        if ($exist["count"] == 0) {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO `users` (`user_name`,`mail`,`password`,`created`,`date_of_birth`) VALUES (:username, :mail, :pass, :created, :dob);";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':username' => $username, ':mail' => $email, ':pass' => $password, ':created' => $date, ':dob' => $birth));
        }
        return false;

    }
    //Changement de mot de passe
    public function changePassword($userid, $password)
    {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $date = date("Y-m-d H:i:s");
        $sql = "UPDATE users SET `password`= :pass, modified = :dat WHERE `user_id` = :userId";
        $req = Bdd::$bddConnection->prepare($sql);
        return $req->execute(array(':userId' => $userid, ':pass' => $password, ":dat" => $date));

    }

    //Ajout d'un vote pour les hebergements
    public function accomodation_vote($user_id, $accommodation_id, $step_id)
    {
        $sql = "SELECT COUNT(*) AS trouve FROM accomodations_votes WHERE accomodations_votes.accomodation_id IN (SELECT accomodations.accomodation_id FROM accomodations WHERE accomodations.step_id = :stepID) AND accomodations_votes.user_id = :userID";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userID' => $user_id, ':stepID' => $step_id));
        $trouve = $result->fetch();
        if ($trouve["trouve"]) {
            $sql = "UPDATE accomodations_votes AS av INNER JOIN accomodations AS a ON a.accomodation_id = av.accomodation_id SET av.accomodation_id = :accID WHERE av.user_id = :userID AND a.step_id = :stepID";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userID' => $user_id, ':accID' => $accommodation_id, ':stepID' => $step_id));
        } else {
            $sql = "INSERT INTO `accomodations_votes` (`user_id`,`accomodation_id`) VALUES (:userId, :accId);";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userId' => $user_id, ':accId' => $accommodation_id));
        }
    }
    //Ajout d'un vote pour les activites
    public function activities_vote($user_id, $activity_id, $step_id)
    {
        $sql = "SELECT COUNT(*) AS trouve FROM activities_votes WHERE activities_votes.activity_id IN (SELECT activities.activity_id FROM activities WHERE activities.step_id = :stepID) AND activities_votes.user_id = :userID";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userID' => $user_id, ':stepID' => $step_id));
        $trouve = $result->fetch();
        if ($trouve["trouve"]) {
            $sql = "UPDATE activities_votes AS av INNER JOIN activities AS a ON a.activity_id = av.activity_id SET av.activity_id = :actID WHERE av.user_id = :userID AND a.step_id = :stepID";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userID' => $user_id, ':actID' => $activity_id, ':stepID' => $step_id));
        } else {
            $sql = "INSERT INTO `activities_votes` (`user_id`,`activity_id`) VALUES (:userId, :actId);";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userId' => $user_id, ':actId' => $activity_id));
        }
    }

    public function has_vote($user_id, $step_id){
        $sql = "SELECT COUNT(*) AS vote FROM `steps_votes` WHERE  `user_id` = :userID AND `step_id` = :stepID";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userID' => $user_id, ':stepID' => $step_id));
        $trouve = $result->fetch();
        return $trouve["vote"];
    }

    //Ajout d'un vote pour les etapes
    public function steps_vote($user_id, $step_id)
    {
        if ($this->has_vote($user_id, $step_id)) {
            $sql = "DELETE FROM steps_votes WHERE user_id = :userId AND step_id = :stepId";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userId' => $user_id, ':stepId' => $step_id));
        } else {
            $sql = "INSERT INTO `steps_votes` (`user_id`,`step_id`) VALUES (:userId, :stepId);";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userId' => $user_id, ':stepId' => $step_id));
        }
    }
    //Ajout d'un vote pour les transports
    public function transports_vote($user_id, $transport_id, $step_id)
    {
        $sql = "SELECT COUNT(*) AS trouve FROM transports_votes WHERE transports_votes.transport_id IN (SELECT transports.transport_id FROM transports WHERE transports.step_id = :stepID) AND transports_votes.user_id = :userID";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userID' => $user_id, ':stepID' => $step_id));
        $trouve = $result->fetch();
        if ($trouve["trouve"]) {
            $sql = "UPDATE transports_votes AS tv INNER JOIN transports AS t ON t.transport_id = tv.transport_id SET tv.transport_id = :transID WHERE tv.user_id = :userID AND t.step_id = :stepID";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userID' => $user_id, ':transID' => $transport_id, ':stepID' => $step_id));
        } else {
            $sql = "INSERT INTO `transports_votes` (`user_id`,`transport_id`) VALUES (:userId, :transId);";
            $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            return $result->execute(array(':userId' => $user_id, ':transId' => $transport_id));
        }
    }

    //Ajout d'un membre dans un voyage
    public function addUserInTrip($user_id, $trip_id)
    {
        $sql = "INSERT INTO `users_trips` (`user_id`,`trip_id`) VALUES (:userId, :tripId);";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':userId' => $user_id, ':tripId' => $trip_id));
    }
    //Suppression d'un membre dans un voyage
    public function deleteUserInTrip($user_id, $trip_id)
    {
        $sql = "DELETE FROM `users_trips` WHERE `user_id` = :userId AND `trip_id` = :tripId";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':userId' => $user_id, ':tripId' => $trip_id));
    }

    //Changement des infos de bases du voyage
    public function changeTripInfo($trip_id, $trip_name = null, $date_beginning = null, $date_end = null, $location = null)
    {
        $params = array(":tripId" => $trip_id);
        $update = "";
        if (null !== $trip_name) {
            $update .= "trip_name = :tripName";
            $params[":tripName"] = $trip_name;
        }
        if (null !== $date_beginning) {
            $update = strlen($update) > 0 ? ", date_beginning = :db" : "date_beginning = :db";
            $params[":db"] = $date_beginning;
        }
        if (null !== $date_end) {
            $update = strlen($update) > 0 ? ", date_end = :de" : "date_end = :de";
            $params[":de"] = $date_end;
        }
        if (null !== $location) {
            $update = strlen($update) > 0 ? ", location = :loc" : "location = :loc";
            $params[":loc"] = $location;
        }

        $sql = "UPDATE trips SET " . $update . " WHERE `trip_id` = :tripId";
        $req = Bdd::$bddConnection->prepare($sql);
        return $req->execute($params);
    }

    //Ajout d'une etape
    public function addStep($step_name, $trip_id, $date_beginning, $date_end)
    {
        $sql = 'INSERT INTO steps (`step_name`, `date_beginning`, `date_end`, `trip_id`) VALUES (:stepname, :db, :de, :tripId)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':stepname' => $step_name, ':db' => $date_beginning, ':de' => $date_end, ':tripId' => $trip_id));
    }

    //Ajout d'une activite
    public function addActivity($activity_name, $step_id, $type, $date_beginning, $date_end, $description, $price = null)
    {
        $sql = 'INSERT INTO activities (`activity_name`, `date_beginning`, `date_end`, `step_id`, `type`, `description`, `price`) VALUES (:activityname, :db, :de, :stepId, :typ, :descr, :price)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':activityname' => $activity_name, ':db' => $date_beginning, ':de' => $date_end, ':stepId' => $step_id, ':typ' => $type, ':descr' => $description, ':price' => $price));
    }
    //Ajout d'un transport
    public function addTransport($origin, $type, $destination, $description, $date_beginning, $date_end, $time_beginning = null, $time_end = null, $price = null, $step_id = null)
    {
        $sql = 'INSERT INTO transports (`step_id`, `type`, `date_beginning`, `date_end`, `time_beginning`, `time_end`, `origin`, `destination`, `description`, `price`) VALUES (:stepId, :typ, :db, :de, :tb, :te, :origin, :dest, :descr, :price)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':stepId' => $step_id, ':typ' => $type, ':db' => $date_beginning, ':de' => $date_end, ':tb' => $time_beginning, ':te' => $time_end, ':origin' => $origin, ':dest' => $destination, ':descr' => $description, ':price' => $price));
    }
    //Ajout d'un hebergement
    public function addAccomodation($acc_name, $step_id, $type, $date_beginning, $date_end, $location, $description, $price = null)
    {
        $sql = 'INSERT INTO accomodations (`accomodation_name`, `step_id`, `date_beginning`, `date_end`, `type`, `location`, `description`, `price`) VALUES (:accname, :stepid, :db, :de, :typ, :loc, :descr, :price)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':stepid' => $step_id, ':typ' => $type, ':db' => $date_beginning, ':de' => $date_end, ':accname' => $acc_name, ':loc' => $location, ':descr' => $description, ':price' => $price));
    }

    //Trouver l'id selon le nom d'utilisateur pour variable de session
    public function findId($username)
    {
        $sql = "SELECT `user_id` as id FROM `users` WHERE `user_name` = :userName";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userName' => $username));
        $user = $result->fetch();
        return $user["id"];
    }

    //Changement des infos de bases du voyage
    public function changeUserInfo($user_id, $username = null, $mail = null, $password = null)
    {
        $date = date("Y-m-d H:i:s");
        $params = array(":user_id" => $user_id, ":date" => $date);
        $update = "";
        if (null !== $username) {
            $update .= "user_name = :username";
            $params[":username"] = $username;
        }
        if (null !== $mail) {
            $update = strlen($update) > 0 ? ", mail = :mail" : "mail = :mail";
            $params[":mail"] = $mail;
        }
        if (null !== $password) {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $update = strlen($update) > 0 ? ", password = :pass" : "password = :pass";
            $params[":pass"] = $password;
        }
        if (strlen($update > 0)) {
            $update .= ", modified = :date";
        }
        $sql = "UPDATE users SET " . $update . " WHERE `user_id` = :user_id";
        $req = Bdd::$bddConnection->prepare($sql);
        return $req->execute($params);
    }

    //Suppression d'un utilisateur
    public function deleteUser($user)
    {
        $sql = 'DELETE FROM `users` WHERE user_id = :userId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':userId' => $user));
    }

    //Avoir étapes avec identifiant voyage
    public function getAllSteps($idVoyage)
    {
        //Jointure obligatoire ?
        $sql = 'SELECT * FROM steps WHERE steps.trip_id = :id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetchAll();
    }

    //Suppression d'un voyage
    public function deleteStep($step_id)
    {
        $sql = 'DELETE FROM `steps` WHERE steps.step_id = :stepId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':stepId' => $step_id));
    }

    //Suppression d'une activité
    public function deleteActivity($activity_id)
    {
        $sql = 'DELETE FROM `activities` WHERE activities.activity_id = :activityId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':activityId' => $activity_id));
    }

    //Suppression d'un voyage
    public function deleteAccomodation($accomodation_id)
    {
        $sql = 'DELETE FROM `accomodations` WHERE accomodations.accomodation_id = :accomodationId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':accomodationId' => $accomodation_id));
    }

    //Suppression d'un voyage
    public function deleteTransport($transport_id)
    {
        $sql = 'DELETE FROM `transports` WHERE transports.transport_id = :transportId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':transportId' => $transport_id));
    }

    //Avoir activités avec identifiant étape
    public function getActivities($idVoyage)
    {
        //Jointure obligatoire ?
        $sql = 'SELECT * FROM activities,steps WHERE steps.trip_id = :id AND activities.step_id=steps.step_id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetchAll();
    }

    //Avoir hébergements avec identifiant voyage
    public function getAccomodations($idVoyage)
    {
        //Jointure obligatoire ?
        $sql = 'SELECT * FROM accomodations,steps WHERE steps.trip_id = :id AND accomodations.step_id=steps.step_id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetchAll();
    }

    //Avoir transports avec identifiant voyage
    public function getTransports($idVoyage)
    {
        //Jointure obligatoire ?
        $sql = 'SELECT * FROM steps,transports WHERE steps.trip_id = :id AND transports.step_id=steps.step_id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetchAll();
    }

    //Avoir tous les utilisateurs
    public function getAllUsers()
    {
        $sql = 'SELECT * FROM users';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute();
        return $result->fetchAll();
    }

    //Avoir tous les utilisateurs
    public function getUsersInTrip($idVoyage)
    {
        $sql = 'SELECT * FROM users_trips,users WHERE users_trips.trip_id = :id AND users_trips.user_id=users.user_id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetchAll();
    }

    //Avoir tous les utilisateurs qui peuvent etre ajouté
    public function getUsersInTripAddable($idVoyage, $idCreator)
    {
        $sql = 'SELECT * FROM users WHERE user_id <> :idC AND user_id NOT IN (SELECT user_id FROM users_trips WHERE users_trips.trip_id = :id)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage, ':idC' => $idCreator));
        return $result->fetchAll();
    }
    //Retourne le si l'utilisateur est dans un voyage (selon un id d'etape)
    public function getUserInTrip($user_id, $step_id)
    {
        $sql = 'SELECT COUNT(*) AS trouve FROM users_trips WHERE users_trips.user_id = :userId AND users_trips.trip_id IN (SELECT trip_id FROM steps WHERE steps.step_id = :stepId)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":userId" => $user_id, ':stepId' => $step_id));
        return $result->fetch();
    }

    //Retourne le nombre d'utilisateur dans un voyage selon l'id d'une etape
    public function getUserCountInTrip($step_id)
    {
        $sql = 'SELECT COUNT(*) AS trouve FROM users_trips WHERE users_trips.trip_id IN (SELECT trip_id FROM steps WHERE steps.step_id = :stepId)';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':stepId' => $step_id));
        return $result->fetch();
    }

    //Retourne les dates d'une etape
    public function getDateEtape($step_id)
    {
        $sql = 'SELECT date_beginning, date_end FROM steps WHERE step_id = :stepId';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':stepId' => $step_id));
        return $result->fetch();
    }

    //Avoir tous les messages et les noms d'utilisateurs concernant un voyage
    public function getMessagesInTrip($idVoyage)
    {
        $sql = 'SELECT * FROM users,messages,messages_trips WHERE messages.user_id = users.user_id AND messages.message_id=messages_trips.message_id AND messages_trips.trip_id=:id ORDER BY message_date ASC';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetchAll();
    }

    //Ajout d'un message dans un voyage
    public function addMessageInTrip($user_id, $trip_id, $content)
    {
        //Evite toutes les injections html, js etc
        $content = htmlspecialchars($content);

        //ajout du message
        $dateActuelle = date("Y-m-d H:i:s");
        $sql = "INSERT INTO `messages` (`user_id`,`content`,`message_date`) VALUES (:userId, :content, :dat);";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':userId' => $user_id, ':content' => $content, ':dat' => $dateActuelle));

        //sélection du message pour l'insérer dans messages_trips
        $sql = 'SELECT message_id FROM messages WHERE messages.user_id = :id AND messages.content=:content AND messages.message_date=:dat';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':id' => $user_id, ':content' => $content, ':dat' => $dateActuelle));
        $message_id = $result->fetch();

        //lien avec le voyage
        $sql = "INSERT INTO `messages_trips` (`message_id`,`trip_id`) VALUES (:messageId, :tripId);";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':messageId' => $message_id[0], ':tripId' => $trip_id));
    }

    //Suppression d'un message dans un voyage
    public function deleteMessageInTrip($message_id)
    {
        $sql = "DELETE FROM `messages_users` WHERE `message_id` = :messageId";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(':messageId' => $message_id));

        $sql = "DELETE FROM `messages` WHERE `message_id` = :messageId";
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        return $result->execute(array(':messageId' => $message_id));
    }

    //Recupere l'id du créateur d'un voyage
    public function getCreatorTrip($idVoyage)
    {
        $sql = 'SELECT creator_id FROM trips WHERE trips.trip_id = :id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetch();
    }

    //Recupere le nom d'un voyage
    public function getNameTrip($idVoyage)
    {
        $sql = 'SELECT trip_name FROM trips WHERE trips.trip_id = :id';
        $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result->execute(array(":id" => $idVoyage));
        return $result->fetch();
    }
}
