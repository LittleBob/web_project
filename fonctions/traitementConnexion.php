<?php
include("./statusCheck.php");
require "./bdd.php";

//Cas de la connexion
if (isset($_POST['nomUtilisateurConnexion']) && isset($_POST['mdpConnexion'])) {
    $nom = $_POST['nomUtilisateurConnexion'];
    $mdp = $_POST['mdpConnexion'];
    $bdd = Bdd::getBdd();
    if ($bdd->connect($nom, $mdp)) {
        //variable de session à ajouter
        $_SESSION['connected'] = true;
        $_SESSION['id'] = $bdd->findId($nom);
       
        header('Location: ../vues/accueil.php');
    } else {
        //Cas mot de passe incorrect
        header('Location: ../vues/connexion.php?loginerror=true');
    }
} else if(isset($_POST['nomUtilisateur']) && isset($_POST['mail']) && isset($_POST['naissance']) && isset($_POST['mdp'])){
    //Cas de l'inscription : on va toujours retouner sur la page connexion pour se connecter
    $nom = $_POST['nomUtilisateur'];
    $mail = $_POST['mail'];
    $naissance = $_POST['naissance'];
    $mdp = $_POST['mdp'];
    $bdd = Bdd::getBdd();
    $verifNom=$bdd->register($nom, $mail, $mdp, $naissance);
    if(!$verifNom){
        header('Location: ../vues/connexion.php?erreur=true');
    }else{
        header('Location: ../vues/connexion.php');
    }
}else{
    //Cas ou il manque quelque chose
    header('Location: ../vues/connexion.php');
}
