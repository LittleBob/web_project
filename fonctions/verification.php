<?php

// pas de vérification des paramètres saisis dans l'url
require "bdd.php";

//Verification que l'utilisateur est le createur du voyage
function administration($idVoyage, $idUtilisateur)
{
    $trouve = false;
    $bdd = Bdd::getBdd();
    $req = $bdd->getAllTrips($idUtilisateur);
    foreach ($req as $row) {
        if ($row['trip_id'] == $idVoyage && $row['creator_id'] == $idUtilisateur) {
            $trouve = true;
        }
    }
    return $trouve;
}
//Verification que l'utilisateur appartient à un voyage à l'aide d'un id voyage
function votes($idVoyage, $idUtilisateur)
{
    $trouve = false;
    $bdd = Bdd::getBdd();
    $req = $bdd->getAllTrips($idUtilisateur);
    foreach ($req as $row) {
        if ($row['trip_id'] == $idVoyage) {
            $trouve = true;
        }
    }
    return $trouve;
}
//Verification que l'utilisateur appartient à un voyage à l'aide d'un id d'etape
function inStep($idStep, $idUtilisateur)
{
    $bdd = Bdd::getBdd();
    return $bdd->getUserInTrip($idUtilisateur, $idStep);
}
