<?php
include "../fonctions/statusCheck.php";
require "../fonctions/verification.php";

if (isset($_GET["id"])) {
    if (inStep($_GET['id'], $_SESSION['id'])) {
        $bdd = Bdd::getBdd();
        $dates = $bdd->getDateEtape($_GET["id"]);
        echo '{"date_beginning" : "'.$dates["date_beginning"].'", "date_end" : "'.$dates["date_end"].'"}';
    }
}
