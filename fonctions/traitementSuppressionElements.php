<?php 
	require "./../fonctions/bdd.php";
	include("../fonctions/statusCheck.php");
 


    if(isset($_POST['etapeSuppression'])){
        $bdd=Bdd::getBdd();
	    $bdd->deleteStep($_POST['etapeSuppression']);

        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }

    if(isset($_POST['activiteSuppression'])){
        $bdd=Bdd::getBdd();
	    $bdd->deleteActivity($_POST['activiteSuppression']);

        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }

    if(isset($_POST['hebergementSuppression'])){
        $bdd=Bdd::getBdd();
	    $bdd->deleteAccomodation($_POST['hebergementSuppression']);

        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }

    if(isset($_POST['transportSuppression'])){
        $bdd=Bdd::getBdd();
	    $bdd->deleteTransport($_POST['transportSuppression']);

        header('Location: ../vues/administrationVoyage.php?id='.$_GET['id']);
    }
	 
			
?>