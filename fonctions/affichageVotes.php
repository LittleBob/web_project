<?php
include "../fonctions/statusCheck.php";
require "../fonctions/verification.php";
if (isset($_GET["id"])) {
    if (inStep($_GET['id'], $_SESSION['id'])) {

        $bdd = Bdd::getBdd();
        $total = $bdd->getUserCountInTrip($_GET["id"])["trouve"] + 1;
        $accomodations = $bdd->getAccomodationsByStep($_GET["id"]);
        $transports = $bdd->getTransportsByStep($_GET["id"]);
        $activities = $bdd->getActivitiesByStep($_GET["id"]);
        $step = $bdd->getStepByStep($_GET["id"]);
        if($step){
        echo "<div name='etapes'/>";

        echo "<table class='table table-striped'><thead>
			<tr>
				<th>Nombre de votes</th>
				<th>Hébergement</th>
				<th>Type</th>
				<th>Date Début</th>
				<th>Date Fin</th>
				<th>Localisation</th>
				<th>Description</th>
				<th>Prix</th>
				<th></th>
			</tr>
		</thead><tbody>";
        foreach ($accomodations as $acc) {
            if (intval($total) >= 1) {
                $percent = (intval($acc["vote"]) / intval($total)) * 100;
            } else {
                $percent = 0;
            }
            echo "<tr accID=" . $acc["accomodation_id"] . "><td>" . $acc['vote'] . " (" . round($percent) . "%)</td><td>" . $acc['accomodation_name'] . "</td><td>" . $acc['type'] . "</td><td>" . $acc['date_beginning'] . "</td><td>" . $acc['date_end'] . "</td><td>" . $acc['location'] . "</td><td>" . $acc['description'] . "</td><td>" . $acc['price'] . "</td><td><button class='btn btn-success' name='acc'>Voter</button></td></tr>";
        }
        echo "</tbody></table>";

        echo "<table class='table table-striped'><thead>
			<tr>
				<th>Nombre de votes</th>
				<th>Transport</th>
				<th>Date début</th>
				<th>Date fin</th>
				<th>Heure début</th>
				<th>Heure fin</th>
				<th>Origine</th>
				<th>Destination</th>
				<th>Description</th>
				<th>Prix</th>
				<th></th>
			</tr>
		</thead><tbody>";
        foreach ($transports as $tran) {
            if (intval($total) >= 1) {
                $percent = (intval($tran["vote"]) / intval($total)) * 100;
            } else {
                $percent = 0;
            }
            echo "<tr transID=" . $tran["transport_id"] . "><td>" . $tran['vote'] . " (" . round($percent) . "%)</td><td>" . $tran['type'] . "</td><td>" . $tran['date_beginning'] . "</td><td>" . $tran['date_end'] . "</td><td>" . $tran['time_beginning'] . "</td><td>" . $tran['time_end'] . "</td><td>" . $tran['origin'] . "</td><td>" . $tran['destination'] . "</td><td>" . $tran['description'] . "</td><td>" . $tran['price'] . "</td><td><button class='btn btn-success' name='trans'>Voter</button></td></tr>";
        }
        echo "</tbody></table>";

        echo "<table class='table table-striped'><thead>
		<tr>
			<th>Nombre de votes</th>
			<th>Activité</th>
			<th>Type</th>
			<th>Date début</th>
			<th>Date fin</th>
			<th>Description</th>
		</tr>
	</thead><tbody>";
        foreach ($activities as $activity) {
            if (intval($total) >= 1) {
                $percent = (intval($activity["vote"]) / intval($total)) * 100;
            } else {
                $percent = 0;
            }
            echo "<tr actID = " . $activity["activity_id"] . "><td>" . $activity['vote'] . " (" . round($percent) . "%)</td><td>" . $activity['activity_name'] . "</td><td>" . $activity['type'] . "</td><td>" . $activity['date_beginning'] . "</td><td>" . $activity['date_end'] . "</td><td>" . $activity['description'] . "</td><td><button class='btn btn-success' name='act'>Voter</button></td></tr>";
        }
        echo "</tbody></table>";
        $voted = $bdd->has_vote($_SESSION['id'], $step['step_id']);
        $text = $voted ? "Retirer le vote" : "Voter pour cette étape";
        $btn = $voted ? "btn-danger" : "btn-success";
        if (intval($total) > 1 OR intval($step["vote"]) > 0) {
            $percent = (intval($step["vote"]) / intval($total)) * 100;
            echo "<div id='avanceeVote'>Nombre de votes de l'étape : " . $step["vote"] . " / " . $total . " (" . round($percent) . "%)</div>";
            echo "<button class='btn $btn' name='step'>$text</button>";
            echo "</div>";
        } else {
            echo "<div id='avanceeVote'>Aucun vote</div>";
            echo "<button class='btn $btn' name='step'>$text</button>";
        }
    }else{
        echo "Aucune étape de disponible";
    }
    } else {
        echo "Interdit";
    }
} else {
    echo "Erreur ID";
}
