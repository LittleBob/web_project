<?php

include "./statusCheck.php";
require "./bdd.php";

if (isset($_POST["transID"]) && isset($_POST["stepID"])) {
    $bdd = Bdd::getBdd();
    $bdd->transports_vote($_SESSION["id"], $_POST["transID"], $_POST["stepID"]);
}
if (isset($_POST["actID"]) && isset($_POST["stepID"])) {
    $bdd = Bdd::getBdd();
    $bdd->activities_vote($_SESSION["id"], $_POST["actID"], $_POST["stepID"]);
}
if (isset($_POST["accID"]) && isset($_POST["stepID"])) {
    $bdd = Bdd::getBdd();
    $bdd->accomodation_vote($_SESSION["id"], $_POST["accID"], $_POST["stepID"]);
}
if (isset($_POST["step"]) && isset($_POST["stepID"])) {
    $bdd = Bdd::getBdd();
    $bdd->steps_vote($_SESSION["id"], $_POST["stepID"]);
}
