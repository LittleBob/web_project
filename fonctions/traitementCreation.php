<?php
    include("./statusCheck.php");
    require "./bdd.php";

    if (isset($_POST['nomVoyage']) && isset($_POST['dateDebut']) && isset($_POST['dateFin'])&& isset($_POST['localisation']) ) {
        $nom = $_POST['nomVoyage'];
        $dateDebut = $_POST['dateDebut'];
        $dateFin = $_POST['dateFin'];
        $loc = $_POST['localisation'];
        $bdd = Bdd::getBdd();
        $bdd->addTrip($nom, $dateDebut, $dateFin, $loc, $_SESSION['id']);
        header('Location: ../vues/accueil.php'); 

    }else{
        //Cas ou il manque quelque chose
        header('Location: ../vues/creationVoyage.php'); 
    }
?>