<?php

include "../fonctions/statusCheck.php";
require "../fonctions/verification.php";
        //affichage du tchat
          
    echo "<ul class='chat'>";
    $bdd = Bdd::getBdd();
    $createur=$bdd->getCreatorTrip($_GET['id']);
    $req=$bdd->getMessagesInTrip($_GET['id']);
    foreach($req as $row)
    {
        $class = "sent";
        if($row["user_id"] == $_SESSION["id"]){
            $class = "replies";
        }
        echo "<li class='{$class}'><p>{$row['content']}<time class='info'>{$row['user_name']} {$row['message_date']} </time></p>";
        //supression de message pour l'administrateur
        if($createur[0]==$_SESSION['id']){          
            echo "<button class='delete' messageId=". $row['message_id'].">Supprimer ce message</button></a></li>";    
        }else{
            echo "</li>";
        }
    }    

    echo "</ul>";

?>