$(()=>{
    $("#dateCache").hide();
})


var btnVoyage = $("#creationVoyage");

//gestion de la date pour l'insertion de voyage
$("#dateDebut").change(() => {
    var dateDebut = $("#dateDebut").val();
    var dateFin = $("#dateFin").val();
    if (dateDebut <= dateFin || dateDebut == "" || dateFin == "") {
        btnVoyage.prop('disabled', false);
        $("#dateCache").hide();
    }else{
        btnVoyage.prop('disabled', true);
        $("#dateCache").show();
    }
});
$("#dateFin").change(() => {
    var dateDebut = $("#dateDebut").val();
    var dateFin = $("#dateFin").val();
    if (dateDebut <= dateFin || dateDebut == "" || dateFin == "") {
        btnVoyage.prop('disabled', false);
        $("#dateCache").hide();
    }else{
        btnVoyage.prop('disabled', true);
        $("#dateCache").show();
    }
});