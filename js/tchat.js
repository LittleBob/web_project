
function maj() {
  var id = $(location).attr("search").split("?id=")[1];
  $.ajax({
    type: 'GET',
    url: `../fonctions/affichageTchat.php?id=${id}`,
    success: function (data) {
      $("#affichageMessages").html(data);
    }
  });
}

setInterval(maj, 3500);

$(() => {
  maj();
  
  $("#ajoutMessage").click(() => {
    var id = $(location).attr("search").split("?id=")[1];
    var content = $("#message").val();
    if (content != "") {
      $.ajax({
        type: 'POST',
        url: `../fonctions/traitementTchatUtilisateur.php`,
        data: { id: id, message: content },
        success: function (data) {
          maj();
        }
      });
      $("#message").val("");
    }
  });

  $("body").on('click', 'button.delete', (e) => {
    console.log("XLCIK");
    var messageId = $(e.target).attr("messageId");
    $.ajax({
      type: 'POST',
      url: `../fonctions/traitementSuppressionMessage.php`,
      data: { id: messageId },
      success: function (data) {
        maj();
      }
    });
  });
});
