$(() => {
    var id = $("#stepSelect").val()
    getData(id);

    $("#stepSelect").change(() => {
        var id = $("#stepSelect").val();
        getData(id);
    });
});

function getData(step_id) {
    $.ajax({
        type: 'GET',
        url: `../fonctions/affichageVotes.php?id=${step_id}`,
        success: function (data) {
            $('#step_detail').html(data);
            votesFunc();
        }
    });
}

function votesFunc() {
    $("[name='trans']").click((e) => {
        var transid = $(e.target).closest("tr").attr("transid");
        var id = $("#stepSelect").val();
        $.ajax({
            type: 'POST',
            data: { transID: transid, stepID: id },
            url: `../fonctions/traitementVotes.php`,
            success: function (data) {
                getData(id);
            }
        });
    });
    $("[name='act']").click((e) => {
        var actid = $(e.target).closest("tr").attr("actid");
        var id = $("#stepSelect").val();
        $.ajax({
            type: 'POST',
            data: { actID: actid, stepID: id },
            url: `../fonctions/traitementVotes.php`,
            success: function (data) {
                getData(id);
            }
        });
    });
    $("[name='acc']").click((e) => {
        var accid = $(e.target).closest("tr").attr("accid");
        var id = $("#stepSelect").val();
        $.ajax({
            type: 'POST',
            data: { accID: accid, stepID: id },
            url: `../fonctions/traitementVotes.php`,
            success: function (data) {
                getData(id);
            }
        });
    });
    $("[name='step']").click((e) => {
        console.log("HERE");
        var id = $("#stepSelect").val();
        $.ajax({
            type: 'POST',
            data: { step: true, stepID: id },
            url: `../fonctions/traitementVotes.php`,
            success: function (data) {
                getData(id);
                console.log(data);
            }
        });
    });
}