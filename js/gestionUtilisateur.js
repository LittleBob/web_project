var btnChangement = $("#validationFormulaire");

$(() => {
    $("#mdpCache").hide();
    $("#mdpCacheTC").hide();
    $("#nouveauMdp").change(() => {
        var nouveau = $("#nouveauMdp").val();
        var confirmation = $("#confMdp").val();
        if (nouveau == confirmation || nouveau == "" || confirmation == "") {
            btnChangement.prop('disabled', false);
            $("#mdpCache").hide();
        } else {
            btnChangement.prop('disabled', true);
            $("#mdpCache").show();
        }
        if (nouveau.length < 8) {
            $("#mdpCacheTC").show();
            btnChangement.prop('disabled', true);
        } else {
            $("#mdpCacheTC").hide();
            btnChangement.prop('disabled', false);
        }
    });
    $("#confMdp").change(() => {
        var nouveau = $("#nouveauMdp").val();
        var confirmation = $("#confMdp").val();
        if (nouveau == confirmation || nouveau == "" || confirmation == "") {
            btnChangement.prop('disabled', false);
            $("#mdpCache").hide();
        } else {
            btnChangement.prop('disabled', true);
            $("#mdpCache").show();
        }

        if (confirmation.length < 8) {
            $("#mdpCacheTC").show();
            btnChangement.prop('disabled', true);
        } else {
            $("#mdpCacheTC").hide();
            btnChangement.prop('disabled', false);
        }
    });
})


function verif() {
    if (confirm("Vous êtes sûr de vouloir désactiver votre compte ? Cette action est irréversible !")) {
        return true;
    } else {
        return false;
    }
}


