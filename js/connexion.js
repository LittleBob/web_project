var btnInscription = $("#btnInscription");
var utilisateur = $("#utilisateurCache");
var mdp = $("#mdpCache");
var identique = $("#identiqueCache");
var verifMail = $("#mailCache");
var verifDate = $("#dateCache");

utilisateur.hide();
mdp.hide();
identique.hide();
verifMail.hide();
verifDate.hide();


$("#nomUtilisateur").change(() => {
    if ($("#nomUtilisateur").val().length < 4) {
        utilisateur.show();
        btnInscription.prop('disabled', true);
    } else {
        utilisateur.hide();
        btnInscription.prop('disabled', false);
    }
});

$("#mdp").change(() => {
    if ($("#mdp").val().length < 8) {
        mdp.show();
        btnInscription.prop('disabled', true);
    } else {
        mdp.hide();
        btnInscription.prop('disabled', false);
        if ($("#confirmation").val() !== $("#mdp").val()) {
            identique.show();
            btnInscription.prop('disabled', true);
        } else {
            identique.hide();
            btnInscription.prop('disabled', false);
        }
    }
});

$("#confirmation").change(() => {
    if ($("#confirmation").val().length > 7) {
        if ($("#confirmation").val() !== $("#mdp").val()) {
            identique.show();
            btnInscription.prop('disabled', true);
        } else {
            identique.hide();
            btnInscription.prop('disabled', false);
        }
    }
});

$('#mail').change(() => {
    var expressionReguliere = /^[a-z0-9\-_\.]+@[a-z0-9]+\.[a-z]{2,5}$/;
    if (expressionReguliere.test($('#mail').val()) == false) {
        verifMail.show();
        btnInscription.prop('disabled', true);
    } else {
        verifMail.hide();
        btnInscription.prop('disabled', false);
    }
});

