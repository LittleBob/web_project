
function changeTabs(evt, tabsName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("nav-link");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabsName).style.display = "block";
    evt.currentTarget.className += " active";
}

function start() {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    document.getElementById("etape").style.display = "block";
}
// On pourrait regrouper ces fonctions mais je trouvais que les arguments étaient trop longs dans le html

function validationSuppression() {
    if (confirm("Vous êtes sûr de vouloir supprimer ce voyage ? Cette action est irréversible !")) {
        return true;
    } else {
        return false;
    }
}

function validationSuppressionEtape() {
    if (confirm("Vous êtes sûr de vouloir supprimer cette étape ? Cela supprimera tous les éléments associés !")) {
        return true;
    } else {
        return false;
    }
}

function validationSuppressionElement($ele) {
    if (confirm("Vous êtes sûr de vouloir supprimer " + $ele + " ? ")) {
        return true;
    } else {
        return false;
    }
}

function getDate(step_id, dates) {
    var result;
    $.ajax({
        type: 'GET',
        url: `../fonctions/infoDateEtape.php?id=${step_id}`,
        async: false,
        success: function (data) {
            result = JSON.parse(data);
        }
    });
    return verifDate(result, dates);
}

function verifDate(dateStep, dates) {
    dateStepBeg = new Date(dateStep.date_beginning);
    dateStepEnd = new Date(dateStep.date_end);
    dateBeg = new Date(dates.date_debut);
    dateEnd = new Date(dates.date_fin);
    if (dateStepBeg > dateBeg || dateEnd > dateStepEnd) {
        return false;
    }
    return true;
}

$(() => {
    var btnEtape = $("#ajoutEtape");
    var btnActivite = $("#ajoutActivite");
    var btnHebergement = $("#ajoutHebergement");
    var btnTransport = $("#ajoutTransport");
    $("#dateEtapeCache").hide();
    $("#dateHebergementCache").hide();
    $("#dateActiviteCache").hide();
    $("#dateTransportCache").hide();
    $("#verificationDateTransport").hide();
    $("#verificationDateActivite").hide();
    $("#verificationDateHebergement").hide();
    $("#dateActiviteCache").hide();
    $("#verificationDateActivite").hide();
    start();
    //gestion de la date pour l'étape
    $("#dateDebutEtape").change(() => {
        var dateDebut = $("#dateDebutEtape").val();
        var dateFin = $("#dateFinEtape").val();
        if (dateDebut >= dateFin || dateDebut == "" || dateFin == "") {
            btnEtape.prop('disabled', true);
            $("#dateEtapeCache").show();
        } else {
            btnEtape.prop('disabled', false);
            $("#dateEtapeCache").hide();
        }
    });
    $("#dateFinEtape").change(() => {
        var dateDebut = $("#dateDebutEtape").val();
        var dateFin = $("#dateFinEtape").val();
        if (dateDebut >= dateFin || dateDebut == "" || dateFin == "") {
            btnEtape.prop('disabled', true);
            $("#dateEtapeCache").show();
        } else {
            btnEtape.prop('disabled', false);
            $("#dateEtapeCache").hide();
        }
    });

    $("#etapeActivite").change(() => {
        id = $("#etapeActivite").val();
        dateDeb = $("#dateDebutActivite").val();
        dateFin = $("#dateFinActivite").val();
        dates = { date_debut: dateDeb, date_fin: dateFin };
        if (getDate(id, dates)) {
            btnActivite.prop('disabled', false);
            $("#verificationDateActivite").hide();
        } else {
            btnActivite.prop('disabled', true);
            $("#verificationDateActivite").show();
        }
    });
    $("#dateDebutActivite").change(() => {
        id = $("#etapeActivite").val();
        dateDeb = $("#dateDebutActivite").val();
        dateFin = $("#dateFinActivite").val();
        if (dateDeb > dateFin || dateDeb == "" || dateFin == "") {
            btnActivite.prop('disabled', true);
            $("#dateActiviteCache").show();
        } else {
            btnActivite.prop('disabled', false);
            $("#dateActiviteCache").hide();
            dates = { date_debut: dateDeb, date_fin: dateFin };
            if (getDate(id, dates)) {
                btnActivite.prop('disabled', false);
                $("#verificationDateActivite").hide();
            } else {
                btnActivite.prop('disabled', true);
                $("#verificationDateActivite").show();
            }
        }
    });
    $("#dateFinActivite").change(() => {
        id = $("#etapeActivite").val();
        dateDeb = $("#dateDebutActivite").val();
        dateFin = $("#dateFinActivite").val();
        if (dateDeb > dateFin || dateDeb == "" || dateFin == "") {
            btnActivite.prop('disabled', true);
            $("#dateActiviteCache").show();
        } else {
            btnActivite.prop('disabled', false);
            $("#dateActiviteCache").hide();
            dates = { date_debut: dateDeb, date_fin: dateFin };
            if (getDate(id, dates)) {
                btnActivite.prop('disabled', false);
                $("#verificationDateActivite").hide();
            } else {
                btnActivite.prop('disabled', true);
                $("#verificationDateActivite").show();
            }
        }
    });

    $("#etapeHebergement").change(() => {
        id = $("#etapeHebergement").val();
        dateDeb = $("#dateDebutHebergement").val();
        dateFin = $("#dateFinHebergement").val();
        dates = { date_debut: dateDeb, date_fin: dateFin };
        if (getDate(id, dates)) {
            btnHebergement.prop('disabled', false);
            $("#verificationDateHebergement").hide();
        } else {
            btnHebergement.prop('disabled', true);
            $("#verificationDateHebergement").show();
        }
    });
    $("#dateDebutHebergement").change(() => {
        id = $("#etapeHebergement").val();
        dateDeb = $("#dateDebutHebergement").val();
        dateFin = $("#dateFinHebergement").val();
        dates = { date_debut: dateDeb, date_fin: dateFin };
        if (dateDeb > dateFin || dateDeb == "" || dateFin == "") {
            btnHebergement.prop('disabled', true);
            $("#dateHebergementCache").show();
        } else {
            btnHebergement.prop('disabled', false);
            $("#dateHebergementCache").hide();
            if (getDate(id, dates)) {
                btnHebergement.prop('disabled', false);
                $("#verificationDateHebergement").hide();
            } else {
                btnHebergement.prop('disabled', true);
                $("#verificationDateHebergement").show();
            }
        }
    });
    $("#dateFinHebergement").change(() => {
        id = $("#etapeHebergement").val();
        dateDeb = $("#dateDebutHebergement").val();
        dateFin = $("#dateFinHebergement").val();
        dates = { date_debut: dateDeb, date_fin: dateFin };
        if (dateDeb > dateFin || dateDeb == "" || dateFin == "") {
            btnHebergement.prop('disabled', true);
            $("#dateHebergementCache").show();
        } else {
            btnHebergement.prop('disabled', false);
            $("#dateHebergementCache").hide();
            if (getDate(id, dates)) {
                btnHebergement.prop('disabled', false);
                $("#verificationDateHebergement").hide();
            } else {
                btnHebergement.prop('disabled', true);
                $("#verificationDateHebergement").show();
            }
        }
    });

   var disable = false;
    $("#etapeTransport").change(() => {
        id = $("#etapeTransport").val();
        if (id != "") {
            dateDeb = $("#dateDebutTransport").val();
            dateFin = $("#dateFinTransport").val();
            dates = { date_debut: dateDeb, date_fin: dateFin };
            if (getDate(id, dates)) {
                btnTransport.prop('disabled', false);
                $("#verificationDateTransport").hide();
                disable = false;
            } else {
                btnTransport.prop('disabled', true);
                $("#verificationDateTransport").show();
                disable = true;
            }
            btnTransport.prop('disabled', disable);
        } else {
            $("#verificationDateTransport").hide();
        }
    });
    $("#dateDebutTransport").change(() => {
        id = $("#etapeTransport").val();
        dateDeb = $("#dateDebutTransport").val();
        dateFin = $("#dateFinTransport").val();
        if (dateDeb > dateFin || dateDeb == "" || dateFin == "") {
            btnTransport.prop('disabled', true);
            $("#dateTransportCache").show();
            disable = true;
        } else {
            btnTransport.prop('disabled', false);
            $("#dateTransportCache").hide();
            disable = false;
        }
        if (id != "") {
            dates = { date_debut: dateDeb, date_fin: dateFin };
            if (getDate(id, dates)) {
                btnTransport.prop('disabled', false);
                $("#verificationDateTransport").hide();
                disable = false;
            } else {
                btnTransport.prop('disabled', true);
                $("#verificationDateTransport").show();
                disable = true;
            }
            btnTransport.prop('disabled', disable);
        } else {
            $("#verificationDateTransport").hide();
        }
    });
    $("#dateFinTransport").change(() => {
        id = $("#etapeTransport").val();
        dateDeb = $("#dateDebutTransport").val();
        dateFin = $("#dateFinTransport").val();
        if (dateDeb > dateFin || dateDeb == "" || dateFin == "") {
            btnTransport.prop('disabled', true);
            $("#dateTransportCache").show();
            disable = true;
        } else {
            btnTransport.prop('disabled', false);
            $("#dateTransportCache").hide();
            disable = false;
        }
        if (id != "") {
            dates = { date_debut: dateDeb, date_fin: dateFin };
            if (getDate(id, dates)) {
                btnTransport.prop('disabled', false);
                $("#verificationDateTransport").hide();
                disable = false;
            } else {
                btnTransport.prop('disabled', true);
                $("#verificationDateTransport").show();
                disable = true;
            }
            
            btnTransport.prop('disabled', disable);
        } else {
            $("#verificationDateTransport").hide();
        }
    });
});