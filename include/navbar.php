<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
    <a class="navbar-brand" href="./accueil.php">
        <img src="../img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
        MyTravel
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="./accueil.php">Mes Voyages</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./creationVoyage.php">Créer un voyage</a>
            </li>
        </ul>
        <a href="./gestionUtilisateur.php"><button class="btn btn-outline-secondary">Mon Compte</button></a>
        <a href="./connexion.php"><button class="btn btn-outline-danger">Déconnexion</button></a>
    </div>
</nav>